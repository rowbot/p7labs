import matplotlib.pyplot as plt
import numpy as np


class Biome:

    def __init__(self, pred_init=1, prey_init=1, tmax=10, dt=0.001):
        self.__dt = dt
        self.__n_iters = int(tmax / dt)
        # Intial state
        self.pred_init = pred_init
        self.prey_init = prey_init
        # Data arrays
        self.time = None
        self.prey = None
        self.pred = None

    def run(self, alpha, beta, delta, gamma):
        """
        Simulation settings:
            alpha - prey growth
            beta - prey mortality
            delta - predator growrh
            gamma - predator mortality
        """
        self.time = np.zeros(self.__n_iters)
        self.prey = np.zeros(self.__n_iters)
        self.pred = np.zeros(self.__n_iters)
        self.prey[0] = self.prey_init
        self.pred[0] = self.pred_init
        # Lotka-Volterra equations
        for i in range(self.__n_iters - 1):
            self.prey[i + 1] = self.prey[i] + self.__dt * self.prey[i] * (alpha - self.pred[i] * beta)
            self.pred[i + 1] = self.pred[i] + self.__dt * self.pred[i] * (delta * self.prey[i] - gamma)
        self.time[1:] = np.add.accumulate([self.__dt] * (self.__n_iters - 1))
        return self.time, self.prey, self.pred

    def plot(self):
        plt.xlabel('time')
        plt.ylabel('n')
        plt.plot(self.time, self.pred, label='predators', color='r')
        plt.plot(self.time, self.prey, label='preys', color='b')
        plt.legend()
        plt.grid()
        plt.show()

    def plot_cycle(self):
        plt.xlabel('n predators')
        plt.ylabel('n preys')
        plt.plot(self.pred, self.prey, color='black')
        plt.grid()
        plt.show()


def main():
    biome = Biome(tmax=10)
    biome.run(2, 5, 2, 2)
    biome.plot()
    biome.plot_cycle()


if __name__ == '__main__':
    main()
